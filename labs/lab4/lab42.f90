! Lab 4, task 1

program task1
  implicit none
  integer :: i1
  integer :: N, display_iteration
  integer :: odd_sum

  open(unit=10,file='data.in')
  read(10,*) N, display_iteration
  close(10)

  odd_sum = 0.d0

  call computeSum(N, display_iteration, odd_sum)

end program task1

!---------------------------------------------------------

subroutine computeSum(N, display_iteration, odd_sum)
  implicit none
  integer :: i1
  integer, intent(in) :: N, display_iteration
  integer, intent(out) :: odd_sum

!Add a do-loop which sums the first N odd integers and prints the result
  !do i1 = 1, N, 2
    !odd_sum = odd_sum + dble(i1)
    if (display_iteration == 1) then
      do i1 = 1, N, 2
        odd_sum = odd_sum + dble(i1)
      print *, 'odd_sum = ',odd_sum
      end do
    else
      do i1 = 1, N, 2
        odd_sum = odd_sum + dble(i1)
        end do
      print *, 'odd_sum = ',odd_sum
    end if

  !end do

  !print *, 'odd_sum = ',odd_sum

end subroutine computeSum

! To compile this code:
! $ gfortran -o task1.exe lab41.f90
! To run the resulting executable: $ ./task1.exe
