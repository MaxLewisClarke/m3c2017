# Assumes lab5 fortran code has been compiled with: $ f2py -c lab5.f90 -m l5
import numpy as np
import matplotlib.pyplot as plt
from l5 import quad

#set parameters, initialze arrays
nvalues = [6,12,24,48]
I_mid = np.zeros(len(nvalues))
I_sim = np.zeros(len(nvalues))

for i in range(len(nvalues)):
    n = nvalues[i]
    quad.quad_n = n
    I_mid[i] = quad.simpson()
    I_sim[i] = quad.simpson()

err_mid = np.abs(np.array(I_mid) - np.arccos(-1))
err_sim = np.abs(np.array(I_sim) - np.arccos(-1))

fig = plt.figure()
plt.clf()
plt.loglog(nvalues, err_mid)
plt.loglog(nvalues, err_sim)
plt.show()
# Plot errors on log-log plots
